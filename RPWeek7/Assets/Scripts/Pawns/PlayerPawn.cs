﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    public PlaneStats Stats;

    public List<WeaponSystem> PrimaryWeapons = new List<WeaponSystem>();

    public List<WeaponSystem> SecondaryWeapons = new List<WeaponSystem>();

    protected new void Start()
    {
        base.Start();
       // Weapons.AddRange(GetComponentsInChildren<WeaponSystem>());
    }

    public override void MoveHorizontal(float dir)
    {
        Vector3 moveVector = tf.right * dir * Stats.RollSpeed * Time.deltaTime;
        
        tf.Translate(moveVector);
    }

    public override void MoveVertical(float dir)
    {
        Vector3 moveVector = tf.up * dir * Stats.ForwardSpeed * Time.deltaTime;

        tf.Translate(moveVector);
    }

    public override void FireWeapon1()
    {
        foreach  (WeaponSystem weapon in PrimaryWeapons)
        {
            weapon.Fire();
        }
    }

    public override void FireWeapon2()
    {
        foreach (WeaponSystem weapon in SecondaryWeapons)
        {
            weapon.Fire();
        }
    }
}
