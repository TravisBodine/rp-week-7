﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    protected Transform tf;
    // Start is called before the first frame update
    protected void Start()
    {
        tf = GetComponent<Transform>();
    }

    public abstract void MoveHorizontal(float dir);
    public abstract void MoveVertical(float dir);
    public abstract void FireWeapon1();
    public abstract void FireWeapon2();
    
    
}
