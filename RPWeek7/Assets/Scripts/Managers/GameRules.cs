﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameRules")]
public class GameRules : ScriptableObject
{
    public Vector3 ScreenBounds;
}
