﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlaneStats")]
public class PlaneStats : ScriptableObject
{
    public float RollSpeed = 10;
    public float ForwardSpeed = 10;
    public float MaxHP = 3;
}
