﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private InputHandler input;

    private Pawn pawn;

    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<InputHandler>();
        pawn = GetComponent<Pawn>();

        input.OnPrimaryFireButtonPressed += pawn.FireWeapon1;
        input.OnSecondaryFireButtonPressed += pawn.FireWeapon2;
    }

    // Update is called once per frame
    void Update()
    {
        if (input.horizontalAxis != 0)
        {
            pawn.MoveHorizontal(input.horizontalAxis);
        }
        if (input.verticalAxis != 0)
        {
            pawn.MoveVertical(input.verticalAxis);
        }
        
    }
}
