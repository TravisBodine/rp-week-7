﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class InputHandler : MonoBehaviour
{
    [HideInInspector] public float horizontalAxis;
    [HideInInspector] public float verticalAxis;

    
    public event Action OnPrimaryFireButtonPressed = delegate { };
    public event Action OnSecondaryFireButtonPressed = delegate { };

    // Update is called once per frame
    void Update()
    {
        horizontalAxis = Input.GetAxisRaw("Horizontal");
        verticalAxis = Input.GetAxisRaw("Vertical");

        if (Input.GetAxisRaw("Fire1") > 0)
        {
            OnPrimaryFireButtonPressed();
        }
        if (Input.GetAxisRaw("Fire2") > 0)
        {
            OnSecondaryFireButtonPressed();
        }
    }
}
