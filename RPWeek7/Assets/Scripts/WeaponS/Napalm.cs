﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Napalm : ProjectileWeapon
{
    public int numberOfStrikes;
    public float timeBetweenStrikes;

    protected override void HandleWeaponFire()
    {
        StartCoroutine(DropNapalm());
    }

    IEnumerator DropNapalm()
    {
        for (int i = 0; i < numberOfStrikes; i++)
        {
            GameObject projectile = Instantiate(Projectile, tf.position, Quaternion.identity);
            projectile.transform.parent = GameManager.instance.ScrollingBackground.transform;

            yield return new WaitForSeconds(timeBetweenStrikes);
        }
    }
}
