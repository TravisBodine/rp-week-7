﻿using UnityEngine;

public class ProjectileWeapon : WeaponSystem
{
    public GameObject Projectile;

    [Tooltip("How many seconds between each shot")]
    public float ROF;

    protected Transform tf;

    float _timer = 0;

    private void Start()
    {
        tf = GetComponent<Transform>();
    }

    /// <summary>
    /// handles everything about fireing the weapon including timer
    /// </summary>
    public override void Fire()
    {
        if(_timer <= Time.time)
        {
            HandleWeaponFire();
            _timer = Time.time + ROF;
        }
    }

    /// <summary>
    /// contains logic for actualy instantiating the bullets
    /// </summary>
    protected virtual void HandleWeaponFire()
    {
       Instantiate(Projectile, tf.position, Quaternion.identity);
        
    }

    
}
