﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : ProjectileWeapon
{
    [Range(1,10)]
    public int NumberOfBullets;
    [Range(1,90)]
    public float angleBetweenBullets;

    protected override void HandleWeaponFire()
    {

        float totalAngle = angleBetweenBullets * (NumberOfBullets - 1);

        float middle = totalAngle / 2;

        for (int i = 0; i < NumberOfBullets ; i++)
        {

            Quaternion bulletAngle = Quaternion.Euler(0,0,middle);
            Instantiate(Projectile, tf.position, bulletAngle);
            middle -= angleBetweenBullets;
        }
    }
}
