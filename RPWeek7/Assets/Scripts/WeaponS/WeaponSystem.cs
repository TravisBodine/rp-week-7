﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  abstract class WeaponSystem : MonoBehaviour
{

    public abstract void Fire();

}
